﻿
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ViewModelPresentation
{
    public class ApplicationViewModel : BaseViewModel
    {
        public bool IsConnecting 
        {
            get => isConnecting;
            set
            {
                isConnecting = value;
                OnPropertyChanged(nameof(IsConnecting));
            }
        }
        private bool isConnecting;
        public bool IsConnected { get; set; }

        public float JoyMeter { get; set; }

        public ChatRoomViewModel ChatRoom { get; set; } = new ChatRoomViewModel();

        public ICommand ConnectCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand DisconnectCommand { get; set; }

        private CancellationTokenSource ConnectionCancellationToken { get; set; }

        public ApplicationViewModel() 
        {
            ConnectCommand = new CommandHandler(async () => await ConnectAsync());
            CancelCommand = new CommandHandler(Cancel);
            DisconnectCommand = new CommandHandler(Disconnect);
        }

        private void Cancel()
        {
            ConnectionCancellationToken?.Cancel();
        }

        private void Disconnect()
        {
            ChatRoom.Clear();
            IsConnected = false;
        }

        private async Task ConnectAsync()
        {
            ConnectionCancellationToken = new CancellationTokenSource();

            await RunCommandAsync(
                () => IsConnecting,
                async () => {

                    await Task
                        .Delay(2000, ConnectionCancellationToken.Token)
                        .ContinueWith(task => IsConnected = !task.IsCanceled);

                    if (IsConnected)
                        ChatRoom.Populate();
                });

            ConnectionCancellationToken.Dispose();
            ConnectionCancellationToken = null;
        }
    }
}
