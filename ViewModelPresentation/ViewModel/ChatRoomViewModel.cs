﻿using System;
using System.Collections.ObjectModel;

namespace ViewModelPresentation
{
    public class ChatRoomViewModel : BaseViewModel
    {
        public ObservableCollection<UserViewModel> Users { get; set; } = new ObservableCollection<UserViewModel>();

        public ChatRoomViewModel() { }

        public void Clear()
        {
            Users.Clear();
        }

        public void Populate()
        {
            Users.Add(new UserViewModel("Thomas Engine", "tank@engine.com", "ffa800"));
            Users.Add(new UserViewModel("Liu Kang", "liu@kang.com", "bdbdbd"));
            Users.Add(new UserViewModel("Reilly Long Nameson", "reilly@long.com", "3099c5"));
            Users.Add(new UserViewModel("Leonardo da Vinci", "leo@vinci.com", "45b6e5"));
            Users.Add(new UserViewModel("Frank the Tank", "frank@tank.com", "ff4747"));
        }
    }
}
