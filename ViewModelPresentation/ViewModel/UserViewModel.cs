﻿namespace ViewModelPresentation
{
    public class UserViewModel : BaseViewModel
    {
        public string Username { get; set; }

        public string PrimaryColor { get; set; }
        
        public string Email { get; set; }

        public UserViewModel()
        {
            Username = "Mr. Placeholder";
            Email = "holder@place.mr";
            PrimaryColor = "FF12AA";
        }

        public UserViewModel(string name, string email, string color)
        {
            Username = name;
            Email = email;
            PrimaryColor = color;
        }
    }
}
