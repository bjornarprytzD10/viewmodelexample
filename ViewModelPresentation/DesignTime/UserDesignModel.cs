﻿namespace ViewModelPresentation
{
    public class UserDesignModel : UserViewModel
    {
        public static UserDesignModel Instance { get; private set; } = new UserDesignModel();

        public UserDesignModel()
        {
            Username = "James Bond";
        }
    }
}
