﻿
using System;
using System.Globalization;
using System.Windows.Media;

namespace ViewModelPresentation
{
    public class ColorToBrushValueConverter : BaseValueConverter<ColorToBrushValueConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (SolidColorBrush)(new BrushConverter().ConvertFrom($"#{value}"));
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
