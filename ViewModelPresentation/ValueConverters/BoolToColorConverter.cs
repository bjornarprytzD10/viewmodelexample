﻿
using System;
using System.Globalization;
using System.Windows.Media;

namespace ViewModelPresentation
{
    public class BoolToColorConverter : BaseValueConverter<BoolToColorConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new SolidColorBrush((bool)value ? Color.FromRgb(255, 0, 0) : Color.FromRgb(0, 255, 0));
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
