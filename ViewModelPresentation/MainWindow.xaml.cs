﻿using System.Windows;

namespace ViewModelPresentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = new ApplicationViewModel();

            InitializeComponent();
        }
    }
}
