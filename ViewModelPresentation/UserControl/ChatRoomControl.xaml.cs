﻿
using System.Windows.Controls;

namespace ViewModelPresentation
{
    /// <summary>
    /// Interaction logic for ChatRoomControl.xaml
    /// </summary>
    public partial class ChatRoomControl : BaseUserControl<ChatRoomViewModel>
    {
        public ChatRoomControl()
        {
            InitializeComponent();
        }
    }
}
