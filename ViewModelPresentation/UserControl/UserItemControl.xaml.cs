﻿
using System.Windows.Controls;

namespace ViewModelPresentation
{
    /// <summary>
    /// Interaction logic for UserItemControl.xaml
    /// </summary>
    public partial class UserItemControl : UserControl
    {
        public UserItemControl()
        {
            InitializeComponent();
        }
    }
}
