﻿
using System.Windows.Controls;

namespace ViewModelPresentation
{
    public class BaseUserControl<VM> : UserControl
        where VM : BaseViewModel, new()
    {
		private VM _viewModel;

		public VM ViewModel
		{
			get => _viewModel;
			set
			{
				if (_viewModel == value) return;

				_viewModel = value;

				DataContext = _viewModel;
			}
		}

		public BaseUserControl()
		{
			ViewModel = new VM();
		}
	}
}
