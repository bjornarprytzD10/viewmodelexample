﻿using System;
using System.Windows;

namespace ViewModelPresentation
{
	public abstract class BaseAttachedProperty<Parent, Property>
		where Parent : new()
	{
		/// <summary>
		/// Fired when the value changes
		/// </summary>
		public event Action<DependencyObject, DependencyPropertyChangedEventArgs> ValueChanged = (sender, e) => { };

		/// <summary>
		/// Singleton instance of the parent class
		/// </summary>
		public static Parent Instance { get; private set; } = new Parent();

		/// <summary>
		/// The Attached Property for this class
		/// </summary>
		public static readonly DependencyProperty ValueProperty = DependencyProperty.RegisterAttached("Value", typeof(Property), typeof(BaseAttachedProperty<Parent, Property>), new PropertyMetadata(new PropertyChangedCallback(OnValuePropertyChanged)));

		/// <summary>
		/// The callback event when the <see cref="ValueProperty"/> is changed
		/// </summary>
		/// <param name="d">The UI element that had its property changed</param>
		/// <param name="e">the arguments for the event</param>
		private static void OnValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			(Instance as BaseAttachedProperty<Parent, Property>)?.OnValueChanged(d, e); // Parent implementation

			(Instance as BaseAttachedProperty<Parent, Property>)?.ValueChanged(d, e);
		}

		public static Property GetValue(DependencyObject d) => (Property)d.GetValue(ValueProperty);
		public static void SetValue(DependencyObject d, Property value) => d.SetValue(ValueProperty, value);

		public virtual void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) { }
	}
}
