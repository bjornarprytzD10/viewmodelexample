﻿using System.Windows.Controls;

namespace ViewModelPresentation
{
	public class BasePage<VM> : Page
		where VM : BaseViewModel, new()
	{
		private VM _viewModel;

		public VM ViewModel
		{
			get => _viewModel;
			set
			{
				if (_viewModel == value) return;

				_viewModel = value;

				DataContext = _viewModel;
			}
		}

		public BasePage()
		{
			ViewModel = new VM();
		}
	}
}
