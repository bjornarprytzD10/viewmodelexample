﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ViewModelPresentation
{
	/// <summary>
	/// Implementation of <see cref="INotifyPropertyChanged"/> to simplify models.
	/// </summary>
	public abstract class BaseViewModel : INotifyPropertyChanged
	{
		/// <summary>
		/// Multicast event for property change notifications.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

		protected object mPropertyValueCheckLock = new object();

		public void OnPropertyChanged(string name)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(name));
		}

		protected async Task RunCommandAsync(Expression<Func<bool>> updatingFlag, Func<Task> action)
		{
			lock (mPropertyValueCheckLock)
			{
				if (updatingFlag.GetPropertyValue())
					return;

				updatingFlag.SetPropertyValue(true);
			}

			try
			{
				await action();
			}
			finally
			{
				updatingFlag.SetPropertyValue(false);
			}
		}


		protected async Task<T> RunCommandAsync<T>(Expression<Func<bool>> updatingFlag, Func<Task<T>> action, T defaultValue = default(T))
		{
			lock (mPropertyValueCheckLock)
			{
				if (updatingFlag.GetPropertyValue())
					return defaultValue;

				updatingFlag.SetPropertyValue(true);
			}

			try
			{
				return await action();
			}
			finally
			{
				updatingFlag.SetPropertyValue(false);
			}
		}
	}
}

